from django.forms import ModelForm
from receipts.models import Receipt, ExpenseCategory, Account


class ReceiptForm(ModelForm):
    class Meta:
        model = Receipt
        fields = [
            "vendor",
            "total",
            "tax",
            "date",
            "category",
            "account",
        ]

    def __init__(self, purchaser=None, **kwargs):
        super(ReceiptForm, self).__init__(**kwargs)
        if purchaser:
            self.fields["category"].queryset = ExpenseCategory.objects.filter(
                owner=purchaser
            )
            self.fields["account"].queryset = Account.objects.filter(
                owner=purchaser
            )


class ExpenseCategoryForm(ModelForm):
    class Meta:
        model = ExpenseCategory
        fields = [
            "name",
        ]


class AccountForm(ModelForm):
    class Meta:
        model = Account
        fields = [
            "name",
            "number",
        ]
